import {ClassFeatures} from "./classFeatures.js"

// Namespace Configuration Values
export const DND5EMOD = {};

// ASCII Artwork
DND5EMOD.ASCII = `_______________________________
______      ______ _____ _____
|  _  \\___  |  _  \\  ___|  ___|
| | | ( _ ) | | | |___ \\| |__
| | | / _ \\/\\ | | |   \\ \\  __|
| |/ / (_>  < |/ //\\__/ / |___
|___/ \\___/\\/___/ \\____/\\____/
_______________________________`;


/**
 * The set of Ability Scores used within the system
 * @type {Object}
 */
DND5EMOD.abilities = {
  "str": "DND5EMOD.AbilityStr",
  "dex": "DND5EMOD.AbilityDex",
  "con": "DND5EMOD.AbilityCon",
  "int": "DND5EMOD.AbilityInt",
  "wis": "DND5EMOD.AbilityWis",
  "cha": "DND5EMOD.AbilityCha"
};

DND5EMOD.abilityAbbreviations = {
  "str": "DND5EMOD.AbilityStrAbbr",
  "dex": "DND5EMOD.AbilityDexAbbr",
  "con": "DND5EMOD.AbilityConAbbr",
  "int": "DND5EMOD.AbilityIntAbbr",
  "wis": "DND5EMOD.AbilityWisAbbr",
  "cha": "DND5EMOD.AbilityChaAbbr"
};

/* -------------------------------------------- */

/**
 * Character alignment options
 * @type {Object}
 */
DND5EMOD.alignments = {
  'lg': "DND5EMOD.AlignmentLG",
  'ng': "DND5EMOD.AlignmentNG",
  'cg': "DND5EMOD.AlignmentCG",
  'ln': "DND5EMOD.AlignmentLN",
  'tn': "DND5EMOD.AlignmentTN",
  'cn': "DND5EMOD.AlignmentCN",
  'le': "DND5EMOD.AlignmentLE",
  'ne': "DND5EMOD.AlignmentNE",
  'ce': "DND5EMOD.AlignmentCE"
};

/* -------------------------------------------- */

/**
 * An enumeration of item attunement types
 * @enum {number}
 */
DND5EMOD.attunementTypes = {
  NONE: 0,
  REQUIRED: 1,
  ATTUNED: 2,
}

/**
 * An enumeration of item attunement states
 * @type {{"0": string, "1": string, "2": string}}
 */
DND5EMOD.attunements = {
  0: "DND5EMOD.AttunementNone",
  1: "DND5EMOD.AttunementRequired",
  2: "DND5EMOD.AttunementAttuned"
};

/* -------------------------------------------- */


DND5EMOD.weaponProficiencies = {
  "sim": "DND5EMOD.WeaponSimpleProficiency",
  "mar": "DND5EMOD.WeaponMartialProficiency"
};

DND5EMOD.toolProficiencies = {
  "art": "DND5EMOD.ToolArtisans",
  "disg": "DND5EMOD.ToolDisguiseKit",
  "forg": "DND5EMOD.ToolForgeryKit",
  "game": "DND5EMOD.ToolGamingSet",
  "herb": "DND5EMOD.ToolHerbalismKit",
  "music": "DND5EMOD.ToolMusicalInstrument",
  "navg": "DND5EMOD.ToolNavigators",
  "pois": "DND5EMOD.ToolPoisonersKit",
  "thief": "DND5EMOD.ToolThieves",
  "vehicle": "DND5EMOD.ToolVehicle"
};


/* -------------------------------------------- */

/**
 * This Object defines the various lengths of time which can occur
 * @type {Object}
 */
DND5EMOD.timePeriods = {
  "inst": "DND5EMOD.TimeInst",
  "turn": "DND5EMOD.TimeTurn",
  "round": "DND5EMOD.TimeRound",
  "minute": "DND5EMOD.TimeMinute",
  "hour": "DND5EMOD.TimeHour",
  "day": "DND5EMOD.TimeDay",
  "month": "DND5EMOD.TimeMonth",
  "year": "DND5EMOD.TimeYear",
  "perm": "DND5EMOD.TimePerm",
  "spec": "DND5EMOD.Special"
};


/* -------------------------------------------- */

/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
DND5EMOD.abilityActivationTypes = {
  "none": "DND5EMOD.None",
  "action": "DND5EMOD.Action",
  "bonus": "DND5EMOD.BonusAction",
  "reaction": "DND5EMOD.Reaction",
  "minute": DND5EMOD.timePeriods.minute,
  "hour": DND5EMOD.timePeriods.hour,
  "day": DND5EMOD.timePeriods.day,
  "special": DND5EMOD.timePeriods.spec,
  "legendary": "DND5EMOD.LegAct",
  "lair": "DND5EMOD.LairAct",
  "crew": "DND5EMOD.VehicleCrewAction"
};

/* -------------------------------------------- */


DND5EMOD.abilityConsumptionTypes = {
  "ammo": "DND5EMOD.ConsumeAmmunition",
  "attribute": "DND5EMOD.ConsumeAttribute",
  "material": "DND5EMOD.ConsumeMaterial",
  "charges": "DND5EMOD.ConsumeCharges"
};


/* -------------------------------------------- */

// Creature Sizes
DND5EMOD.actorSizes = {
  "tiny": "DND5EMOD.SizeTiny",
  "sm": "DND5EMOD.SizeSmall",
  "med": "DND5EMOD.SizeMedium",
  "lg": "DND5EMOD.SizeLarge",
  "huge": "DND5EMOD.SizeHuge",
  "grg": "DND5EMOD.SizeGargantuan"
};

DND5EMOD.tokenSizes = {
  "tiny": 1,
  "sm": 1,
  "med": 1,
  "lg": 2,
  "huge": 3,
  "grg": 4
};

/* -------------------------------------------- */

/**
 * Classification types for item action types
 * @type {Object}
 */
DND5EMOD.itemActionTypes = {
  "mwak": "DND5EMOD.ActionMWAK",
  "rwak": "DND5EMOD.ActionRWAK",
  "msak": "DND5EMOD.ActionMSAK",
  "rsak": "DND5EMOD.ActionRSAK",
  "save": "DND5EMOD.ActionSave",
  "heal": "DND5EMOD.ActionHeal",
  "abil": "DND5EMOD.ActionAbil",
  "util": "DND5EMOD.ActionUtil",
  "other": "DND5EMOD.ActionOther"
};

/* -------------------------------------------- */

DND5EMOD.itemCapacityTypes = {
  "items": "DND5EMOD.ItemContainerCapacityItems",
  "weight": "DND5EMOD.ItemContainerCapacityWeight"
};

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability
 * @type {Object}
 */
DND5EMOD.limitedUsePeriods = {
  "sr": "DND5EMOD.ShortRest",
  "lr": "DND5EMOD.LongRest",
  "day": "DND5EMOD.Day",
  "charges": "DND5EMOD.Charges"
};


/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
DND5EMOD.equipmentTypes = {
  "light": "DND5EMOD.EquipmentLight",
  "medium": "DND5EMOD.EquipmentMedium",
  "heavy": "DND5EMOD.EquipmentHeavy",
  "bonus": "DND5EMOD.EquipmentBonus",
  "natural": "DND5EMOD.EquipmentNatural",
  "shield": "DND5EMOD.EquipmentShield",
  "clothing": "DND5EMOD.EquipmentClothing",
  "trinket": "DND5EMOD.EquipmentTrinket",
  "vehicle": "DND5EMOD.EquipmentVehicle"
};


/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
DND5EMOD.armorProficiencies = {
  "lgt": DND5EMOD.equipmentTypes.light,
  "med": DND5EMOD.equipmentTypes.medium,
  "hvy": DND5EMOD.equipmentTypes.heavy,
  "shl": "DND5EMOD.EquipmentShieldProficiency"
};


/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system
 * @type {Object}
 */
DND5EMOD.consumableTypes = {
  "ammo": "DND5EMOD.ConsumableAmmunition",
  "potion": "DND5EMOD.ConsumablePotion",
  "poison": "DND5EMOD.ConsumablePoison",
  "food": "DND5EMOD.ConsumableFood",
  "scroll": "DND5EMOD.ConsumableScroll",
  "wand": "DND5EMOD.ConsumableWand",
  "rod": "DND5EMOD.ConsumableRod",
  "trinket": "DND5EMOD.ConsumableTrinket"
};

/* -------------------------------------------- */

/**
 * The valid currency denominations supported by the 5e system
 * @type {Object}
 */
DND5EMOD.currencies = {
  "pp": "DND5EMOD.CurrencyPP",
  "gp": "DND5EMOD.CurrencyGP",
  "ep": "DND5EMOD.CurrencyEP",
  "sp": "DND5EMOD.CurrencySP",
  "cp": "DND5EMOD.CurrencyCP",
};


/**
 * Define the upwards-conversion rules for registered currency types
 * @type {{string, object}}
 */
DND5EMOD.currencyConversion = {
  cp: {into: "sp", each: 10},
  sp: {into: "ep", each: 5 },
  ep: {into: "gp", each: 2 },
  gp: {into: "pp", each: 10}
};

/* -------------------------------------------- */


// Damage Types
DND5EMOD.damageTypes = {
  "acid": "DND5EMOD.DamageAcid",
  "bludgeoning": "DND5EMOD.DamageBludgeoning",
  "cold": "DND5EMOD.DamageCold",
  "fire": "DND5EMOD.DamageFire",
  "force": "DND5EMOD.DamageForce",
  "lightning": "DND5EMOD.DamageLightning",
  "necrotic": "DND5EMOD.DamageNecrotic",
  "piercing": "DND5EMOD.DamagePiercing",
  "poison": "DND5EMOD.DamagePoison",
  "psychic": "DND5EMOD.DamagePsychic",
  "radiant": "DND5EMOD.DamageRadiant",
  "slashing": "DND5EMOD.DamageSlashing",
  "thunder": "DND5EMOD.DamageThunder"
};

// Damage Resistance Types
DND5EMOD.damageResistanceTypes = mergeObject(duplicate(DND5EMOD.damageTypes), {
  "physical": "DND5EMOD.DamagePhysical"
});


/* -------------------------------------------- */

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @type {Object<string,string>}
 */
DND5EMOD.movementTypes = {
  "burrow": "DND5EMOD.MovementBurrow",
  "climb": "DND5EMOD.MovementClimb",
  "fly": "DND5EMOD.MovementFly",
  "swim": "DND5EMOD.MovementSwim",
  "walk": "DND5EMOD.MovementWalk",
}

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @type {Object<string,string>}
 */
DND5EMOD.movementUnits = {
  "ft": "DND5EMOD.DistFt",
  "mi": "DND5EMOD.DistMi"
}

/**
 * The valid units of measure for the range of an action or effect.
 * This object automatically includes the movement units from DND5EMOD.movementUnits
 * @type {Object<string,string>}
 */
DND5EMOD.distanceUnits = {
  "none": "DND5EMOD.None",
  "self": "DND5EMOD.DistSelf",
  "touch": "DND5EMOD.DistTouch",
  "spec": "DND5EMOD.Special",
  "any": "DND5EMOD.DistAny"
};
for ( let [k, v] of Object.entries(DND5EMOD.movementUnits) ) {
  DND5EMOD.distanceUnits[k] = v;
}

/* -------------------------------------------- */


/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
DND5EMOD.encumbrance = {
  currencyPerWeight: 50,
  strMultiplier: 15,
  vehicleWeightMultiplier: 2000 // 2000 lbs in a ton
};

/* -------------------------------------------- */

/**
 * This Object defines the types of single or area targets which can be applied
 * @type {Object}
 */
DND5EMOD.targetTypes = {
  "none": "DND5EMOD.None",
  "self": "DND5EMOD.TargetSelf",
  "creature": "DND5EMOD.TargetCreature",
  "ally": "DND5EMOD.TargetAlly",
  "enemy": "DND5EMOD.TargetEnemy",
  "object": "DND5EMOD.TargetObject",
  "space": "DND5EMOD.TargetSpace",
  "radius": "DND5EMOD.TargetRadius",
  "sphere": "DND5EMOD.TargetSphere",
  "cylinder": "DND5EMOD.TargetCylinder",
  "cone": "DND5EMOD.TargetCone",
  "square": "DND5EMOD.TargetSquare",
  "cube": "DND5EMOD.TargetCube",
  "line": "DND5EMOD.TargetLine",
  "wall": "DND5EMOD.TargetWall"
};


/* -------------------------------------------- */


/**
 * Map the subset of target types which produce a template area of effect
 * The keys are DND5EMOD target types and the values are MeasuredTemplate shape types
 * @type {Object}
 */
DND5EMOD.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};


/* -------------------------------------------- */

// Healing Types
DND5EMOD.healingTypes = {
  "healing": "DND5EMOD.Healing",
  "temphp": "DND5EMOD.HealingTemp"
};


/* -------------------------------------------- */


/**
 * Enumerate the denominations of hit dice which can apply to classes
 * @type {Array.<string>}
 */
DND5EMOD.hitDieTypes = ["d6", "d8", "d10", "d12"];


/* -------------------------------------------- */

/**
 * The set of possible sensory perception types which an Actor may have
 * @type {object}
 */
DND5EMOD.senses = {
  "blindsight": "DND5EMOD.SenseBlindsight",
  "darkvision": "DND5EMOD.SenseDarkvision",
  "tremorsense": "DND5EMOD.SenseTremorsense",
  "truesight": "DND5EMOD.SenseTruesight"
};

/* -------------------------------------------- */

/**
 * The set of skill which can be trained
 * @type {Object}
 */
DND5EMOD.skills = {
  "acr": "DND5EMOD.SkillAcr",
  "ani": "DND5EMOD.SkillAni",
  "arc": "DND5EMOD.SkillArc",
  "ath": "DND5EMOD.SkillAth",
  "dec": "DND5EMOD.SkillDec",
  "his": "DND5EMOD.SkillHis",
  "ins": "DND5EMOD.SkillIns",
  "itm": "DND5EMOD.SkillItm",
  "inv": "DND5EMOD.SkillInv",
  "med": "DND5EMOD.SkillMed",
  "nat": "DND5EMOD.SkillNat",
  "prc": "DND5EMOD.SkillPrc",
  "prf": "DND5EMOD.SkillPrf",
  "per": "DND5EMOD.SkillPer",
  "rel": "DND5EMOD.SkillRel",
  "slt": "DND5EMOD.SkillSlt",
  "ste": "DND5EMOD.SkillSte",
  "sur": "DND5EMOD.SkillSur"
};


/* -------------------------------------------- */

DND5EMOD.spellPreparationModes = {
  "prepared": "DND5EMOD.SpellPrepPrepared",
  "pact": "DND5EMOD.PactMagic",
  "always": "DND5EMOD.SpellPrepAlways",
  "atwill": "DND5EMOD.SpellPrepAtWill",
  "innate": "DND5EMOD.SpellPrepInnate"
};

DND5EMOD.spellUpcastModes = ["always", "pact", "prepared"];

DND5EMOD.spellProgression = {
  "none": "DND5EMOD.SpellNone",
  "full": "DND5EMOD.SpellProgFull",
  "half": "DND5EMOD.SpellProgHalf",
  "third": "DND5EMOD.SpellProgThird",
  "pact": "DND5EMOD.SpellProgPact",
  "artificer": "DND5EMOD.SpellProgArt"
};

/* -------------------------------------------- */

/**
 * The available choices for how spell damage scaling may be computed
 * @type {Object}
 */
DND5EMOD.spellScalingModes = {
  "none": "DND5EMOD.SpellNone",
  "cantrip": "DND5EMOD.SpellCantrip",
  "level": "DND5EMOD.SpellLevel"
};

/* -------------------------------------------- */


/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
DND5EMOD.weaponTypes = {
  "simpleM": "DND5EMOD.WeaponSimpleM",
  "simpleR": "DND5EMOD.WeaponSimpleR",
  "martialM": "DND5EMOD.WeaponMartialM",
  "martialR": "DND5EMOD.WeaponMartialR",
  "natural": "DND5EMOD.WeaponNatural",
  "improv": "DND5EMOD.WeaponImprov",
  "siege": "DND5EMOD.WeaponSiege"
};


/* -------------------------------------------- */

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
DND5EMOD.weaponProperties = {
  "ada": "DND5EMOD.WeaponPropertiesAda",
  "amm": "DND5EMOD.WeaponPropertiesAmm",
  "fin": "DND5EMOD.WeaponPropertiesFin",
  "fir": "DND5EMOD.WeaponPropertiesFir",
  "foc": "DND5EMOD.WeaponPropertiesFoc",
  "hvy": "DND5EMOD.WeaponPropertiesHvy",
  "lgt": "DND5EMOD.WeaponPropertiesLgt",
  "lod": "DND5EMOD.WeaponPropertiesLod",
  "mgc": "DND5EMOD.WeaponPropertiesMgc",
  "rch": "DND5EMOD.WeaponPropertiesRch",
  "rel": "DND5EMOD.WeaponPropertiesRel",
  "ret": "DND5EMOD.WeaponPropertiesRet",
  "sil": "DND5EMOD.WeaponPropertiesSil",
  "spc": "DND5EMOD.WeaponPropertiesSpc",
  "thr": "DND5EMOD.WeaponPropertiesThr",
  "two": "DND5EMOD.WeaponPropertiesTwo",
  "ver": "DND5EMOD.WeaponPropertiesVer"
};


// Spell Components
DND5EMOD.spellComponents = {
  "V": "DND5EMOD.ComponentVerbal",
  "S": "DND5EMOD.ComponentSomatic",
  "M": "DND5EMOD.ComponentMaterial"
};

// Spell Schools
DND5EMOD.spellSchools = {
  "abj": "DND5EMOD.SchoolAbj",
  "con": "DND5EMOD.SchoolCon",
  "div": "DND5EMOD.SchoolDiv",
  "enc": "DND5EMOD.SchoolEnc",
  "evo": "DND5EMOD.SchoolEvo",
  "ill": "DND5EMOD.SchoolIll",
  "nec": "DND5EMOD.SchoolNec",
  "trs": "DND5EMOD.SchoolTrs"
};

// Spell Levels
DND5EMOD.spellLevels = {
  0: "DND5EMOD.SpellLevel0",
  1: "DND5EMOD.SpellLevel1",
  2: "DND5EMOD.SpellLevel2",
  3: "DND5EMOD.SpellLevel3",
  4: "DND5EMOD.SpellLevel4",
  5: "DND5EMOD.SpellLevel5",
  6: "DND5EMOD.SpellLevel6",
  7: "DND5EMOD.SpellLevel7",
  8: "DND5EMOD.SpellLevel8",
  9: "DND5EMOD.SpellLevel9"
};

// Spell Scroll Compendium UUIDs
DND5EMOD.spellScrollIds = {
  0: 'Compendium.dnd5emod.items.rQ6sO7HDWzqMhSI3',
  1: 'Compendium.dnd5emod.items.9GSfMg0VOA2b4uFN',
  2: 'Compendium.dnd5emod.items.XdDp6CKh9qEvPTuS',
  3: 'Compendium.dnd5emod.items.hqVKZie7x9w3Kqds',
  4: 'Compendium.dnd5emod.items.DM7hzgL836ZyUFB1',
  5: 'Compendium.dnd5emod.items.wa1VF8TXHmkrrR35',
  6: 'Compendium.dnd5emod.items.tI3rWx4bxefNCexS',
  7: 'Compendium.dnd5emod.items.mtyw4NS1s7j2EJaD',
  8: 'Compendium.dnd5emod.items.aOrinPg7yuDZEuWr',
  9: 'Compendium.dnd5emod.items.O4YbkJkLlnsgUszZ'
};

/**
 * Define the standard slot progression by character level.
 * The entries of this array represent the spell slot progression for a full spell-caster.
 * @type {Array[]}
 */
DND5EMOD.SPELL_SLOT_TABLE = [
  [2],
  [3],
  [4, 2],
  [4, 3],
  [4, 3, 2],
  [4, 3, 3],
  [4, 3, 3, 1],
  [4, 3, 3, 2],
  [4, 3, 3, 3, 1],
  [4, 3, 3, 3, 2],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 2, 1, 1]
];

/* -------------------------------------------- */

// Polymorph options.
DND5EMOD.polymorphSettings = {
  keepPhysical: 'DND5EMOD.PolymorphKeepPhysical',
  keepMental: 'DND5EMOD.PolymorphKeepMental',
  keepSaves: 'DND5EMOD.PolymorphKeepSaves',
  keepSkills: 'DND5EMOD.PolymorphKeepSkills',
  mergeSaves: 'DND5EMOD.PolymorphMergeSaves',
  mergeSkills: 'DND5EMOD.PolymorphMergeSkills',
  keepClass: 'DND5EMOD.PolymorphKeepClass',
  keepFeats: 'DND5EMOD.PolymorphKeepFeats',
  keepSpells: 'DND5EMOD.PolymorphKeepSpells',
  keepItems: 'DND5EMOD.PolymorphKeepItems',
  keepBio: 'DND5EMOD.PolymorphKeepBio',
  keepVision: 'DND5EMOD.PolymorphKeepVision'
};

/* -------------------------------------------- */

/**
 * Skill, ability, and tool proficiency levels
 * Each level provides a proficiency multiplier
 * @type {Object}
 */
DND5EMOD.proficiencyLevels = {
  0: "DND5EMOD.NotProficient",
  1: "DND5EMOD.Proficient",
  0.5: "DND5EMOD.HalfProficient",
  2: "DND5EMOD.Expertise"
};

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object.
 * In cases where multiple pieces of cover are
 * in play, we take the highest value.
 */
DND5EMOD.cover = {
  0: 'DND5EMOD.None',
  .5: 'DND5EMOD.CoverHalf',
  .75: 'DND5EMOD.CoverThreeQuarters',
  1: 'DND5EMOD.CoverTotal'
};

/* -------------------------------------------- */


// Condition Types
DND5EMOD.conditionTypes = {
  "blinded": "DND5EMOD.ConBlinded",
  "charmed": "DND5EMOD.ConCharmed",
  "deafened": "DND5EMOD.ConDeafened",
  "diseased": "DND5EMOD.ConDiseased",
  "exhaustion": "DND5EMOD.ConExhaustion",
  "frightened": "DND5EMOD.ConFrightened",
  "grappled": "DND5EMOD.ConGrappled",
  "incapacitated": "DND5EMOD.ConIncapacitated",
  "invisible": "DND5EMOD.ConInvisible",
  "paralyzed": "DND5EMOD.ConParalyzed",
  "petrified": "DND5EMOD.ConPetrified",
  "poisoned": "DND5EMOD.ConPoisoned",
  "prone": "DND5EMOD.ConProne",
  "restrained": "DND5EMOD.ConRestrained",
  "stunned": "DND5EMOD.ConStunned",
  "unconscious": "DND5EMOD.ConUnconscious"
};

// Languages
DND5EMOD.languages = {
  "common": "DND5EMOD.LanguagesCommon",
  "aarakocra": "DND5EMOD.LanguagesAarakocra",
  "abyssal": "DND5EMOD.LanguagesAbyssal",
  "aquan": "DND5EMOD.LanguagesAquan",
  "auran": "DND5EMOD.LanguagesAuran",
  "celestial": "DND5EMOD.LanguagesCelestial",
  "deep": "DND5EMOD.LanguagesDeepSpeech",
  "draconic": "DND5EMOD.LanguagesDraconic",
  "druidic": "DND5EMOD.LanguagesDruidic",
  "dwarvish": "DND5EMOD.LanguagesDwarvish",
  "elvish": "DND5EMOD.LanguagesElvish",
  "giant": "DND5EMOD.LanguagesGiant",
  "gith": "DND5EMOD.LanguagesGith",
  "gnomish": "DND5EMOD.LanguagesGnomish",
  "goblin": "DND5EMOD.LanguagesGoblin",
  "gnoll": "DND5EMOD.LanguagesGnoll",
  "halfling": "DND5EMOD.LanguagesHalfling",
  "ignan": "DND5EMOD.LanguagesIgnan",
  "infernal": "DND5EMOD.LanguagesInfernal",
  "orc": "DND5EMOD.LanguagesOrc",
  "primordial": "DND5EMOD.LanguagesPrimordial",
  "sylvan": "DND5EMOD.LanguagesSylvan",
  "terran": "DND5EMOD.LanguagesTerran",
  "cant": "DND5EMOD.LanguagesThievesCant",
  "undercommon": "DND5EMOD.LanguagesUndercommon"
};

// Character Level XP Requirements
DND5EMOD.CHARACTER_EXP_LEVELS =  [
  0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000,
  120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000]
;

// Challenge Rating XP Levels
DND5EMOD.CR_EXP_LEVELS = [
  10, 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000,
  20000, 22000, 25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000
];

// Character Features Per Class And Level
DND5EMOD.classFeatures = ClassFeatures;

// Configure Optional Character Flags
DND5EMOD.characterFlags = {
  "diamondSoul": {
    name: "DND5EMOD.FlagsDiamondSoul",
    hint: "DND5EMOD.FlagsDiamondSoulHint",
    section: "Feats",
    type: Boolean
  },
  "elvenAccuracy": {
    name: "DND5EMOD.FlagsElvenAccuracy",
    hint: "DND5EMOD.FlagsElvenAccuracyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "halflingLucky": {
    name: "DND5EMOD.FlagsHalflingLucky",
    hint: "DND5EMOD.FlagsHalflingLuckyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "initiativeAdv": {
    name: "DND5EMOD.FlagsInitiativeAdv",
    hint: "DND5EMOD.FlagsInitiativeAdvHint",
    section: "Feats",
    type: Boolean
  },
  "initiativeAlert": {
    name: "DND5EMOD.FlagsAlert",
    hint: "DND5EMOD.FlagsAlertHint",
    section: "Feats",
    type: Boolean
  },
  "jackOfAllTrades": {
    name: "DND5EMOD.FlagsJOAT",
    hint: "DND5EMOD.FlagsJOATHint",
    section: "Feats",
    type: Boolean
  },
  "observantFeat": {
    name: "DND5EMOD.FlagsObservant",
    hint: "DND5EMOD.FlagsObservantHint",
    skills: ['prc','inv'],
    section: "Feats",
    type: Boolean
  },
  "powerfulBuild": {
    name: "DND5EMOD.FlagsPowerfulBuild",
    hint: "DND5EMOD.FlagsPowerfulBuildHint",
    section: "Racial Traits",
    type: Boolean
  },
  "reliableTalent": {
    name: "DND5EMOD.FlagsReliableTalent",
    hint: "DND5EMOD.FlagsReliableTalentHint",
    section: "Feats",
    type: Boolean
  },
  "remarkableAthlete": {
    name: "DND5EMOD.FlagsRemarkableAthlete",
    hint: "DND5EMOD.FlagsRemarkableAthleteHint",
    abilities: ['str','dex','con'],
    section: "Feats",
    type: Boolean
  },
  "weaponCriticalThreshold": {
    name: "DND5EMOD.FlagsWeaponCritThreshold",
    hint: "DND5EMOD.FlagsWeaponCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  },
  "spellCriticalThreshold": {
    name: "DND5EMOD.FlagsSpellCritThreshold",
    hint: "DND5EMOD.FlagsSpellCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  },
  "meleeCriticalDamageDice": {
    name: "DND5EMOD.FlagsMeleeCriticalDice",
    hint: "DND5EMOD.FlagsMeleeCriticalDiceHint",
    section: "Feats",
    type: Number,
    placeholder: 0
  },
};

// Configure allowed status flags
DND5EMOD.allowedActorFlags = ["isPolymorphed", "originalActor"].concat(Object.keys(DND5EMOD.characterFlags));
